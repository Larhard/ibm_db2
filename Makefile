PDF_VIEWER = evince
PDFLATEX = pdflatex
MKDIR = mkdir -p

TMP_DIR := .tmp

SOURCES = $(wildcard *.tex)
TARGETS = $(patsubst %.tex,%,$(SOURCES))
AUTODEPS = $(patsubst %.tex,$(TMP_DIR)/%.md,$(SOURCES))

LAST_MAKE = $(TMP_DIR)/last.mk

-include $(LAST_MAKE)

dir_guard = @$(MKDIR) $(@D)


save_cmd_goals :
	@$(MKDIR) `dirname $(LAST_MAKE)`
ifneq (,$(MAKECMDGOALS))
	@echo -e 'LAST_GOALS = $(MAKECMDGOALS)' > $(LAST_MAKE)
else ifneq (,$(LAST_GOALS))
	@echo -e 'LAST_GOALS = $(LAST_GOALS)' > $(LAST_MAKE)
else
	@echo -e 'last :\n\t@echo nothing to do' > $(LAST_MAKE)
endif

	
$(TARGETS) : % : %.pdf save_cmd_goals
	$(PDF_VIEWER) $<

%.pdf %.md : | save_cmd_goals


%.pdf : %.tex
	$(PDFLATEX) $<
	@$(PDFLATEX) $<
	@$(PDFLATEX) $<

$(LAST_MAKE) : save_cmd_goals

last : $(LAST_GOALS)


.PHONY : last save_cmd_goals $(TARGETS)
.DEFAULT_GOAL = last
